﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class KbartItem
    {
        //public KbartItem headers = new KbartItem{
        //    print_identifier = "print_identifier",
        //    online_identifier = "online_identifier",
        //    date_first_issue_online = "date_first_issue_online",
        //    date_last_issue_online = "date_last_issue_online",
        //    publication_title = "publication_title"
        //};
        public string print_identifier { get; set; }
        public string online_identifier { get; set; }
        public string date_first_issue_online { get; set; }
        public string date_last_issue_online { get; set; }
        public string publication_title { get; set; }

        public KbartItem getHeaders() {
            return new KbartItem{
                print_identifier = "print_identifier",
                online_identifier = "online_identifier",
                date_first_issue_online = "date_first_issue_online",
                date_last_issue_online = "date_last_issue_online",
                publication_title = "publication_title"
            };
        }

        public String filedToString(char delimiter = '\t')
        {
            String str;
            str = publication_title;
            str += delimiter + print_identifier;
            str += delimiter + online_identifier;
            str += delimiter + date_first_issue_online;
            str += delimiter + date_last_issue_online;
            return str;
        }
    }





}
