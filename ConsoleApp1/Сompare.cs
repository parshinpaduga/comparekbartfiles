﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1
{
    class Сompare
    {
        private NationalKbart National;
        private AllKbart All;
        private UcertainKbart Ucertain;
        private ResultFileKbart ResultFile;


        public Сompare(string fileNameNational, string fileNameAll) {
            National = new NationalKbart(fileNameNational);
            All = new AllKbart(fileNameAll);
            Ucertain = new UcertainKbart(@"D:\TempFiles\parseKbart\ucertain.txt");
            ResultFile = new ResultFileKbart(@"D:\TempFiles\parseKbart\resultFile.txt");
        }
        //сравнение двух KBART файлов
        //первый National, может быть неполным, содержит список нацональной подписки
        //второй All, должен содержать всю возможную информацию об журналах издательства
        //результат ResultFile содержит пересечение. Собранный файл с полями для загрузки через админку
        //результат Ucertain - список ненайденых журналов
        //
        public void diffNationalToAll() {
            if (All.KbartItems.Count == 0) return;
            if (National.KbartItems.Count > 0) {

            foreach (var nat in National.KbartItems) {
                    var kbartInfo = getFindKbart(nat);
                    if (kbartInfo != null)
                    {
                        ResultFile.KbartItems.Add(kbartInfo);
                    }
                    else {
                        Ucertain.KbartItems.Add(nat);
                        ResultFile.KbartItems.Add(nat);
                    }
                }
            }
            ResultFile.SaveFile();
            Ucertain.SaveFile();
        }

        private KbartItem getFindKbart(KbartItem nat) {

            KbartItem found = All.KbartItems.Find(item => item.publication_title == nat.publication_title);
                if (found != null) return found;

            found = All.KbartItems.Find(item => item.publication_title == nat.publication_title.Replace("&", "And"));
                if (found != null) return found;

            if (!string.IsNullOrEmpty(nat.print_identifier)){
                found = All.KbartItems.Find(item => item.print_identifier == nat.print_identifier);
                if (found != null) return found;
            }
            if (!string.IsNullOrEmpty(nat.online_identifier)) {
                found = All.KbartItems.Find(item => item.online_identifier == nat.online_identifier);
                if (found != null) return found;
            }


            return null;
        }





    }
}
