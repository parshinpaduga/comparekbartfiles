﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


namespace ConsoleApp1
{
    class Kbart
    {
        protected string FileName;
        private readonly Regex RgYears = new Regex("\\d{4}");
        public List<KbartItem> KbartItems = new List<KbartItem>();

        public Kbart() {
           // this.FileName = @"D:\Проекты\зарубежные издательства\Подписка19\Sage\SageNational19.txt";
        }

        protected void OpenFile()
        {
            Console.WriteLine("Разбираем файл: " + FileName);

            var fileContent = File.ReadAllLines(FileName);

            var isFirstLine = true;
            int issn1 = 0, issn2 = 0, date1 = 0, date2 = 0, title = 0;

            foreach (var line in fileContent)
            {
                if (string.IsNullOrEmpty(line))
                    continue;

                var ss = line.Split('\t');
                KbartItem item = new KbartItem();

                if (isFirstLine)
                {
                    title = Array.IndexOf(ss, "publication_title");
                    issn1 = Array.IndexOf(ss, "print_identifier");
                    issn2 = Array.IndexOf(ss, "online_identifier");
                    date1 = Array.IndexOf(ss, "date_first_issue_online");
                    date2 = Array.IndexOf(ss, "date_last_issue_online");

                    KbartItems.Add(item.getHeaders());
                    isFirstLine = false;
                    continue;
                }
                int? yend;
                var ystart = yend = null;

                if (title >= 0)  item.publication_title = ss[title];
                if (issn1 >= 0) item.print_identifier = ss[issn1];
                else item.print_identifier = "";
                if (issn2 >= 0) item.online_identifier = ss[issn2];
                else item.online_identifier = "";
                if (date1 >= 0) {
                    var m = RgYears.Match(ss[date1]);
                    if (m.Success)
                    {
                        ystart = Convert.ToInt32(m.Value);   
                    }
                }
                if (date2 >= 0)
                {
                    var m = RgYears.Match(ss[date2]);
                    if (m.Success)
                    {
                        ystart = Convert.ToInt32(m.Value);    
                    }
                }
                item.date_first_issue_online = ystart.ToString();
                item.date_last_issue_online = yend.ToString(); ;
                KbartItems.Add(item);
                isFirstLine = false;

            }
        }


        public void SaveFile()
        {
            List<String> fileOutList = new List<String>();
            foreach (KbartItem item in KbartItems) {
                fileOutList.Add(item.filedToString());
            }
            File.WriteAllLines(FileName, fileOutList.ToArray());
        }



        } 

}
